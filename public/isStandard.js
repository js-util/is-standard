(function () {
  "use strict"
  const specialAttr = "is-standard";
  class IsStandardElement extends HTMLElement {
    connectedCallback() {
      setTimeout(() => {
        //setTimeout of 0 within connectedCallback allows this.innerHTML
        let newEl = document.createElement(this.attributes[specialAttr].value);
        [...this.attributes].forEach(attr => {
          this.removeAttribute(attr.nodeName);
          if (attr.nodeName !== specialAttr) {
            newEl.setAttribute(attr.nodeName, attr.nodeValue);
          }
        });
        newEl.setAttribute(this.tagName.toLowerCase(), "")
        newEl.innerHTML = this.innerHTML;
        this.outerHTML = newEl.outerHTML;
      });
    }
  }
  let main = () => {
    new Set([... document.querySelectorAll("["+specialAttr+"]:not(:defined)")]
      .map(el => el.tagName.toLowerCase())).forEach(elName => {
        customElements.define(elName, class extends IsStandardElement {});
    });
  };
  let whenReady = fn => {
    if (document.readyState != "loading") {
      fn();
    } else {
      document.addEventListener("DOMContentLoaded", fn);
    }
  };
  whenReady(main);
})();
