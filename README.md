# isStandard.js

*Lightweight HTML5 custom elements*

---

And a utility that provides support for the `is-standard` attribute.


### &#x25B8;  Solution

The `is` attribute is an official HTML5 standard &mdash; and the only recommended way to extend built-in elements. However, only custom elements may use the `is` attribute. (Support for the official standard is also not expected in Safari):

```html
<button is="custom-button"></button>        <!-- ✅ standard -->
<custom-button is="button"></custom-button> <!-- ❌ unsupported -->
```

The `is-standard` implementation provides functionality analogous to the `is` attribute, but with ergonomics closer to regular, autonomous custom elements:

```html
<Custom-Button is-standard="button"></Custom-Button> <!-- ✨ isStandard.js -->
```

---

### &#x25B8;  Styling

Styling fully compatible with any standard framework:

```css
button { background-color: orange } /* as normal */
```

Styling specific to custom elements:

```css
[Custom-Button] { background-color: green }
```

View the [Example](https://js-util.gitlab.io/is-standard) for more information.

---

### &#x25B8;  Strengths

At present, the `is-standard` attribute is well-suited for several tasks:

* A quick element rename, for better source code clarity

* Lightweight reusable components, sans boilerplate

* Interoperability with many libraries
  
  

Simply include the script file:

```html
<head>
<!-- ... -->
<script src="isStandard.js"></script>
</head>
```

No other scripting is necessary. An option for programmatically activating the attribute is not currently provided.


More future directions might be: a mixin or direct inheritance API to enable integration with even the most full-featured components &ndash; and possibly full  compatibility with the existing `is` attribute.

---

License: BSD (2-Clause)
